# Jenkins
Everything needed to run a Jenkins Master.

Whether you're looking for `Dockerfiles` to build production images or just a local dev environment to experiment on without risk of bringing down the production CI, this repo has got you covered.



## Requirements
- [VirtualBox](https://download.virtualbox.org/virtualbox/6.0.4/)
- [Vagrant](https://www.vagrantup.com/downloads.html) (v2.2+) - _Virtualization helper for developers_
    - **_(recommended)_** [`vagrant-vbguest`](https://github.com/dotless-de/vagrant-vbguest) plugin (`vagrant plugin install vagrant-vbguest --plugin-version 0.17.2`)
        - Resolves many common issues encountered with VirtualBox as provider.
    - _(optional)_ [`vagrant-hostsupdater`](https://github.com/cogitatio/vagrant-hostsupdater) plugin (`vagrant plugin install vagrant-hostsupdater`)
        - Automatically manages your hosts file on `vagrant up`, `vagrant halt`, etc.



## Getting Started
1. Run `./startup.sh`.
1. Open [http://jenkins.test](http://jenkins.test)
1. Enter the `initialAdminPassword` from `docker_volumes/jenkins_home/secrets/initialAdminPassword`
1. Enjoy!



## Usage
### Plugins
There are 3 ways to manage plugins: Jenkins GUI, Jenkins CLI, or Ansible.

#### Jenkins GUI
1. Manage Jenkins > Manage Plugins > Available
1. Search

#### Jenkins CLI
1. Install Jenkins-CLI.jar to the Jenkins home dir.
1. `java -jar /var/jenkins_home/jenkins-cli.jar -s http://localhost:8080/ -auth <YOUR_JENKINS_USERNAME>:<YOUR_JENKINS_PASSWORD> install-plugin <PLUGIN_NAME>`

#### Ansible
A wrapper around installing/using Jenkins CLI is supplied with this repo as a convenience improve its ease of use.  It also attempts to add things like idempotency to allow for robust automation opportunities.

By default, the `manage-plugins.yml` playbook installs the `blueocean` plugin but is easily customized. 

```bash
(cd provision && vagrant ssh)  # SSH into Vagrant

cd /provision && ansible-playbook \
	--inventory inventories/vagrant.yml \
	--extra-vars='jenkins_username={username} jenkins_password={password}' \
manage-plugins.yml
```



## How it works
### Simple
The `startup.sh` will do the following things:

1. Offer to automatically remove any artifacts from previous executions.
1. Create a new Debian VM in VirtualBox.
1. Configure that VM to host Docker containers.
1. Build a custom image based on `jenkins/jenkins:latest`.
1. Run that image.

The tools provided here give you a containerized Jenkins Master that can spin up additional containers to use as ad-hoc, custom build agents.


### Detailed
If you are looking for more detail than what the above explanation provides, here is a more in-depth breakdown:

#### Startup
1. Spin up a Debian VM and install Docker on it.
    - This will be running the Docker daemon, acting as the "Docker host" that is actually running all Docker containers for this project.
    - _Alternatively, create an actual Debian server with your favorite hosting provider._
1. Build a custom Docker image for our Jenkins Master from within the Debian VM.
    - The VM's available images may viewed at any time by simply running `docker image ls` from within the VM.
    - This Docker image is based on `docker_tags/latest/Dockerfile` and is _literally_ just these 3 things added together:
        - Upstream `jenkins/jenkins:latest`
        - +`docker` CLI (**not the daemon**--just the CLI)
        - +allow the `jenkins` shell user to run `docker` CLI commands
1. Start the Jenkins Master container (with reasonable defaults).
    - It essentially "takes over" the VM's HTTP ports (80/443), so it is viewable with a web browser at `<your VM's IP address>`.
        - If you have `vagrant-hostsupdater` installed, [jenkins.test](http://jenkins.test) is a more convenient alias of that IP via your Vagrant host's `hosts` file.

#### Builds
If a `Jenkinsfile` contains a step specifying a Docker container as its `agent`, that means that--somewhere along the line--a command like `docker run [image]` will be executed.

Since `/var/run/docker.sock` is mounted from the VM into the Jenkins Master (JM), that means that it will resolve like so: `pipeline agent -> 'docker command' -> /var/run/docker.sock (JM) -> /var/run/docker.sock (VM)`.

This means that the following changes happen on the VM:
```bash
# before build ran
$ docker container ls
NAMES
jenkins-master

# while build is running
$ docker container ls
NAMES
jenkins-master
my-build-container

# after build is finished
$ docker container ls
NAMES
jenkins-master
```
