#! /usr/bin/env bash
#
# Install necessary tools on Debian VM for provisioning
#
# This script was created to essentially replace Vagrant's `config.vm.provision :ansible_local`...
# ...because it does not support running Ansible Galaxy with SSH key forwarding; therefore, Galaxy...
# ...will fail when trying to install roles from any PRIVATE repos since the SSH key auth will fail.
#
# Syntax: ./provision-local.sh [Pip-compatible ansible_version]
#
# Usage:
# - Default: ./provision-local.sh
# - Custom:  ./provision-local.sh "~=2.6"
#

ansible_version="${1:-~=2.9.2}"



echo "Prepare to install Ansible on guest..."
if ! sudo apt-get install -y git python3-pip; then
	echo "Preparations for Ansible installation on guest failed."
	exit 1
fi


echo "Installing Ansible on guest..."
if ! sudo pip3 install "ansible${ansible_version}"; then
	echo "Ansible installation on guest failed."
	exit 1
fi


# echo "Installing provisioning dependencies..."
# if ! (cd /provision && ansible-galaxy install -r requirements.yml); then
# 	echo "Provisioning dependencies failed to install."
# 	exit 1
# fi


echo "Provisioning guest VM..."
if ! (cd /provision && ansible-playbook -i inventories/vagrant.yml provision.yml -vv); then
	echo "Guest provisioning failed."
	exit 1
else
	echo "Provisioned guest successfully."
fi
