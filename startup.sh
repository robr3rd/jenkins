#! /usr/bin/env bash
#
# Spin up a full VM, make it a Docker Machine, build the Jenkins Master image, and start the container
#


cd provision || exit 1  # everything happens from there


# Offer to clean up previous installations, and do so if desired
echo -n 'Would you like to check for and remove any previous installations? (y/N): '
read -r is_cleanup

if [ "$is_cleanup" == 'y' ] || \
	[ "$is_cleanup" == 'Y' ] || \
	[ "$is_cleanup" == 'yes' ] || \
	[ "$is_cleanup" == 'YES' ];
then
	echo 'INFO: Initiating clean up process'

	# Remove the VM
	echo 'INFO: Destroying VM'
	vagrant destroy || exit 1

	# Clean up old data files
	echo 'INFO: Removing Jenkins data from Docker Volume on host'
	rm -rf ../docker_volumes/jenkins_home || exit 1

	echo 'SUCCESS: Clean up finished'
fi


# Create the VM
echo 'INFO: Creating VM'
vagrant up || exit 1

# Convert the VM to a Docker Machine
echo 'INFO: Install Docker on guest'
vagrant ssh -c '/provision/provision-local.sh' || exit 1

echo "INFO: Adding 'vagrant' user to 'docker' group to allow it to use Docker without 'sudo'"
vagrant ssh -c 'sudo usermod -aG docker vagrant' || exit 1

# Build the image (reminder: now on the Docker Machine)
echo 'INFO: Building custom Jenkins Master image'
# Note: 'DOCKER_HOST_GID' is set dynamically to match the GID in Vagrant.  # usually 999; often 998; sometimes different
vagrant ssh -c 'docker build \
	--tag robr3rd/jenkins-master:latest \
	--build-arg DOCKER_HOST_GID="$(grep "^docker" /etc/group | cut -d":" -f3)" \
/var/docker_tags/latest' || exit 1

# Run Jenkins
echo 'INFO: Running custom Jenkins Master image'
vagrant ssh -c 'docker run -d -p 80:8080 -p 443:8443 -p 50000:50000 -v /var/run/docker.sock:/var/run/docker.sock -v /var/docker_volumes/jenkins/jenkins_home:/var/jenkins_home --name=jenkins-master robr3rd/jenkins-master:latest' || exit 1

echo 'SUCCESS: Done!  You may access your Jenkins Master at http://jenkins.test!'



# Below is the same exact command, but rewritten for readability with long flag names
#
# docker run \
# 	--detach \
# 	--name=jenkins-master \
# 	--env DOCKER_SOCKET_GID=998 \  # must match `grep '^docker' /etc/group` on the *Docker host* to allow socket access
# 	--publish 80:8080 \  # reminder: all ports are getting mapped **to the VM (Vagrant)**
# 	--publish 443:8443 \  # required if providing HTTPS; may be omitted otherwise
# 	--publish 50000:50000 \  # this is "the Jenkins port" by default, and it is used for agent <-> master communication
# 	--volume /var/run/docker.sock:/var/run/docker.sock \  # allow agents to create containers via host's Docker socket
# 	--volume /var/docker_volumes/jenkins/jenkins_home:/var/jenkins_home \  # keep configs safe in a reusable volume
# robr3rd/jenkins-master:latest
